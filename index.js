//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");

//Khai báo thư viện path
const path = require("path");
//Khởi tạo app express
const app = express();

//Khai báo cổng của project 
const port = 8000;

//Khai báo sử dụng các tài nguyên static (images, js, css v..v..)
app.use(express.static("views"));

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    //response.send("<h1 style='color:red'>Hello world</h1>");
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/index.html'));
})

app.get("/about", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/about.html'));
})

app.get("/sitemap", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/sitemap.html'));
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})